package daniyar;

public class SortingApp {

    /**
     * Sorts raw array in natural order.
     *
     * @param input with non-sorted elements
     * @return sorted array with ASC sorting
     */
    public int[] sort(int[] input) {
        if (input.length == 0 || input == null) {
            throw new IllegalArgumentException("Input is empty");
        }
        int temporary;
        int length = input.length;

        for (int i = 0; i < length; i++) {
            for (int j = i + 1; j < length; j++) {
                if (input[i] > input[j]) {
                    temporary = input[i];
                    input[i] = input[j];
                    input[j] = temporary;
                }
            }
        }
        return input;
    }
}
