package daniyar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class Main {

    public static int[] result;
    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        test();
    }

    public static void test() {
        int[] args = new int[]{1, 3, 4, 7, 8};
        SortingApp app = new SortingApp();
        int[] sort = app.sort(args);
        System.out.println(Arrays.toString(sort));
    }

    public static void old(String[] args) {
        System.out.println("Hello, user!");
        logger.trace("We've just greeted the user!");
        logger.debug("We've just greeted the user!");
        logger.info("We've just greeted the user!");
        logger.warn("We've just greeted the user!");
        logger.error("We've just greeted the user!");
        logger.fatal("We've just greeted the user!");

        int[] newArgs = new int[args.length];

        for (int i = 0; i < args.length; i++) {
            newArgs[i] = Integer.parseInt(args[i]);
        }

        int temporary = 0;
        int length = newArgs.length;
        for (int i = 0; i < length; i++) {
            for (int j = i + 1; j < length; j++) {
                if (newArgs[i] > newArgs[j]) {
                    temporary = newArgs[i];
                    newArgs[i] = newArgs[j];
                    newArgs[j] = temporary;
                }
            }
        }
        result = newArgs;
        // System.out.println(Arrays.toString(Arrays.stream(newArgs).toArray()));
    }
}