package daniyar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingTest {
    private int[] args;

    private SortingApp app = new SortingApp();

    @Parameterized.Parameters
    public static List<int[]> data() {
        return Arrays.asList(new int[]{5, 4, 2, 3, 1});
    }

    public SortingTest(int[] args) {
        this.args = args;
    }

    @Test
    public void positiveNumbersSortedCorrectly() {
        int[] expected = new int[]{1, 2, 3, 4, 5};
        int[] actual = app.sort(args);

        assertArrayEquals(actual, expected);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenParametersNotPassedExceptionShouldBeThrown(){
        int[] input = new int[]{};
        app.sort(input);
    }

    @Test
    public void testSingleElementArrayCase() {
        int[] input = new int[] { -4 };
        app.sort(input);
        assertArrayEquals(input, new int[] { -4 });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullCase(){
        int[] input = null;
        app.sort(input);
    }

    @Test
    public void testSortedArraysCase() {
        int[] input = new int[] {-9,-5,1,3,8,12 };
        app.sort(input);
        assertArrayEquals(input, new int[] {-9,-5,1,3,8,12 });
    }

}
